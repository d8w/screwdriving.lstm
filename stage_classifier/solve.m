dataFilename='../data/screwdriving_stage_type.mat';
rng_seed=20171129;
modelFilename='screwdriving_stage_lstm_model.mat';

%% Load data
load(dataFilename);
Y=YStage;


%% Options

% Data split options
opt.splitTrainPct = .6;
opt.splitValPct = .2;
opt.splitTestPct = .2;

% LSTM options
opt.lstmInputSize = 24;
opt.lstmOutputSize = 100;
opt.lstmNumClasses = 7; % stages


%% Specify the training options

options = trainingOptions('sgdm', ...
    'InitialLearnRate',0.0001,...
    'MaxEpochs',200, ...
    'MiniBatchSize',27, ...
    'Shuffle', 'every-epoch');

opt.options = options;


%% Train

accu = solver(X, Y, rng_seed, modelFilename, opt)
