function acc = solver(X,Y,rng_seed,modelFilename,opt)
%SOLVER Summary of this function goes here
%   Detailed explanation goes here


%% Divide the data to three sets: train, validation, test

rng(rng_seed) % for reproducibility
[trainInd, valInd, testInd] = dividerand(size(X,1) , opt.splitTrainPct, ...
    opt.splitValPct, opt.splitTestPct);
XTrain = X(trainInd);
YTrain = Y(trainInd);
XVal = X(valInd);
YVal = Y(valInd);
XTest = X(testInd);
YTest = Y(testInd);


%% Sort Data by Sequence Length

numObservations = numel(XTrain);
sequenceLengths = zeros(numObservations, 1);
for i=1:numObservations
    sequence = XTrain{i};
    sequenceLengths(i) = size(sequence,2);
end


%% Sort the data by sequence length.

[sequenceLengths,idx] = sort(sequenceLengths);
XTrain = XTrain(idx);
YTrain = YTrain(idx);


%% Define LSTM Network Architecture

inputSize = opt.lstmInputSize;
outputSize = opt.lstmOutputSize;
outputMode = 'last';
numClasses = opt.lstmNumClasses;

layers = [ ...
    sequenceInputLayer(inputSize)
    lstmLayer(outputSize,'OutputMode',outputMode)
    fullyConnectedLayer(numClasses)
    softmaxLayer
    classificationLayer]


%% Specify the training options

options = opt.options;


%% Train LSTM Network

[net, traininfo] = trainNetwork(XTrain,YTrain,layers,options);


%% Save LSTM Network
filename=modelFilename;
save(filename, 'net', 'traininfo', 'opt');
disp(['Saved the trained network to ', filename])


%% Test LSTM Network

numObservationsTest = numel(XTest);
sequenceLengthsTest = zeros(numObservationsTest, 1);
for i=1:numObservationsTest
    sequence = XTest{i};
    sequenceLengthsTest(i) = size(sequence,2);
end
[sequenceLengthsTest,idx] = sort(sequenceLengthsTest);
XTest = XTest(idx);
YTest = YTest(idx);


%% Classify the test data.

YPred = classify(net,XTest, ...
    'MiniBatchSize',options.MiniBatchSize);


%% Calculate the classification accuracy of the predictions.

acc = sum(YPred == YTest)./numel(YTest);

end

