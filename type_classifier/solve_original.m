%% Load data
dataFilename='../data/screwdriving_original_class_type_wgn.mat';
rng_seed=20171129;
modelFilename='screwdriving_original_type_lstm_model.mat';


%% Configure training options
% Data split options
opt.splitTrainPct = .6;
opt.splitValPct = .2;
opt.splitTestPct = .2;

% LSTM options
opt.lstmInputSize = 8;
opt.lstmOutputSize = 100;
opt.lstmNumClasses = 7;


%% Specify the training options

options = trainingOptions('sgdm', ...
    'InitialLearnRate',0.0001, ...
    'MaxEpochs',1200, ...
    'MiniBatchSize',27, ...
    'Shuffle', 'never', ...
    'CheckpointPath', 'snapshot');

opt.options = options;


%% Train
accu = solver(dataFilename, rng_seed, modelFilename, opt)
