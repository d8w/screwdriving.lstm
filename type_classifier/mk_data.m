%% Load data from csv and produce X and Y of training and validation.

root='/data/foxconn/foxconn/screwdriving_data/imputed/';
if exist(root, 'dir') ~= 7
    disp(['No such directory: ', root])
    return
end

src=strcat(root, '*.csv');
X = cell(1862,1);
Xorig = cell(1862,1);
Y = zeros(1862,1);
files = dir(src);
for k = 1:length(files)
    baseFileName = files(k).name;
    fullFileName = fullfile(root, baseFileName);
    M = readtable(fullFileName);
    X{k} = M{:, 4:end}';
    Xorig{k} = M{:, 4:11}';
    Y(k) = M{1, 3};
end

Y = categorical(Y);

filename='screwdriving_class_type.mat';
save(filename, 'X', 'Y');
disp(['Saved data to ', filename])

filename='screwdriving_original_class_type.mat';
X=Xorig;
save(filename, 'X', 'Y');
disp(['Saved data to ', filename])