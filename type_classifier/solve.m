dataFilename='../data/screwdriving_class_type_wgn.mat';
rng_seed=20171129;
modelFilename='screwdriving_type_lstm_model.mat';

% Data split options
opt.splitTrainPct = .6;
opt.splitValPct = .2;
opt.splitTestPct = .2;

% LSTM options
opt.lstmInputSize = 24;
opt.lstmOutputSize = 100;
opt.lstmNumClasses = 8;

% Training options
opt.maxEpochs = 20;
opt.miniBatchSize = 27;
opt.shuffle = 'never';

%% Specify the training options

maxEpochs = opt.maxEpochs;
miniBatchSize = opt.miniBatchSize;
shuffle = opt.shuffle;

options = trainingOptions('sgdm', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'Shuffle', shuffle, ...
    'Plots','training-progress');

accu = solver(dataFilename, rng_seed, modelFilename, opt)