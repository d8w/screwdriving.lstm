%% Load data from csv and produce X and Y of training and validation.

root='/data/foxconn/foxconn/screwdriving_data/imputed';
if exist(root, 'dir') ~= 7
    disp(['No such directory: ', root])
    return
end

src=strcat(root, '*.csv');
X = cell(1862,1);
Y = zeros(1862,1);
files = dir(src);
for k = 1:length(files)
    baseFileName = files(k).name;
    fullFileName = fullfile(root, baseFileName);
    M = readtable(fullFileName);
    X{k} = M{:, 4:end}';
    Y(k) = M{1, 2};
end

Y = categorical(Y);

save('screwdriving.mat', 'X', 'Y')
