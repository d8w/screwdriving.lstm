%% Load data
load('screwdriving.mat')


%% Divide the data to three sets: train, validation, test

rng(20171129) % for reproducibility
[trainInd, valInd, testInd] = dividerand(size(X,1) , 0.6, 0.2, 0.2);
XTrain = X(trainInd);
YTrain = Y(trainInd);
XVal = X(valInd);
YVal = Y(valInd);
XTest = X(testInd);
YTest = Y(testInd);


%% Sort Data by Sequence Length

numObservations = numel(XTrain);
sequenceLengths = zeros(numObservations, 1);
for i=1:numObservations
    sequence = XTrain{i};
    sequenceLengths(i) = size(sequence,2);
end


%% Sort the data by sequence length.

[sequenceLengths,idx] = sort(sequenceLengths);
XTrain = XTrain(idx);
YTrain = YTrain(idx);


%% Define LSTM Network Architecture

inputSize = 24;
outputSize = 100;
outputMode = 'last';
numClasses = 2;

layers = [ ...
    sequenceInputLayer(inputSize)
    lstmLayer(outputSize,'OutputMode',outputMode)
    fullyConnectedLayer(numClasses)
    softmaxLayer
    classificationLayer]


%% Specify the training options

maxEpochs = 200;
miniBatchSize = 25;
shuffle = 'never';

options = trainingOptions('sgdm', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'Shuffle', shuffle);


%% Train LSTM Network

[net, traininfo] = trainNetwork(XTrain,YTrain,layers,options);


%% Save LSTM Network
filename='screwdriving_lstm_model.mat';
save(filename, 'net', 'traininfo');
disp(['Saved the trained network to ', filename])

%% Test LSTM Network

numObservationsTest = numel(XTest);
sequenceLengthsTest = zeros(numObservationsTest, 1);
for i=1:numObservationsTest
    sequence = XTest{i};
    sequenceLengthsTest(i) = size(sequence,2);
end
[sequenceLengthsTest,idx] = sort(sequenceLengthsTest);
XTest = XTest(idx);
YTest = YTest(idx);


%% Classify the test data.

YPred = classify(net,XTest, ...
    'MiniBatchSize',miniBatchSize);


%% Calculate the classification accuracy of the predictions.

acc = sum(YPred == YTest)./numel(YTest)
