%% Load data

load('screwdriving.mat')


%% View the first five observations.

X(1:5)


%% Visualize the first time series in a plot. 
%  Each subplot corresponds to a feature.

numFeatures = size(X{1},1);
figure
for i = 1:numFeatures
    subplot(numFeatures,1,numFeatures+1-i)
    plot(X{1}(i,:));
    ylabel(i) 
    xticklabels('')
    yticklabels('')
    box off
end
title("Training Observation 1")
subplot(numFeatures,1,numFeatures)
xticklabels('auto')
xlabel("Time Step")


%% Sort Data by Sequence Length

% Get the sequence lengths for each observation.
numObservations = numel(X);
sequenceLengths = zeros(numObservations, 1);
for i=1:numObservations
    sequence = X{i};
    sequenceLengths(i) = size(sequence,2);
end

% Sort the data by sequence length.

[sequenceLengths,idx] = sort(sequenceLengths);
X = X(idx);
Y = Y(idx);

% View the sorted sequence lengths in a bar chart.

longestObservation = max(cellfun('length', X));

figure
bar(sequenceLengths)
ylim([0 longestObservation])
xlabel("Sequence")
ylabel("Length")
title("Sequence Lengths")

% Choose a mini-batch size of N to divide the training data evenly and 
% reduce the amount of padding in the mini-batches.
% Determine the mini-batch boundaries.

miniBatchSize = 25;
miniBatchLocations = miniBatchSize+1:miniBatchSize:numObservations;
XLocations = repmat(miniBatchLocations,[2 1]);
YLocations = repmat([0;longestObservation],[1 size(XLocations,2)]);

% Plot the mini-batch boundaries with the sequence lengths. 
% The plot shows how sequences are divided into mini-batches.

hold on
line(XLocations,YLocations, ...
        'Color','r', ...
        'LineStyle','--')