%% Load data

load('screwdriving.mat')


%% Load the trained model

load('screwdriving_lstm_model.mat')


%% Divide the data to three sets: train, validation, test

rng(20171129) % for reproducibility
[trainInd, valInd, testInd] = dividerand(size(X,1) , 0.6, 0.2, 0.2);
XTrain = X(trainInd);
YTrain = Y(trainInd);
XVal = X(valInd);
YVal = Y(valInd);
XTest = X(testInd);
YTest = Y(testInd);


%% Test LSTM Network
% Sort Data by Sequence Length
% During training, the software splits the training data into mini-batches 
% and pads or truncates the sequences so that they have the same length. 
% Too much padding or discarding of data can have a negative impact on 
% the network performance.

numObservationsTest = numel(XTest);
sequenceLengthsTest = zeros(numObservationsTest, 1);
for i=1:numObservationsTest
    sequence = XTest{i};
    sequenceLengthsTest(i) = size(sequence,2);
end
[sequenceLengthsTest,idx] = sort(sequenceLengthsTest);
XTest = XTest(idx);
YTest = YTest(idx);


%% Classify the test data.

miniBatchSize = 25;
YPred = classify(net,XTest, ...
    'MiniBatchSize',miniBatchSize);


%% Calculate the classification accuracy of the predictions.

acc = sum(YPred == YTest)./numel(YTest)