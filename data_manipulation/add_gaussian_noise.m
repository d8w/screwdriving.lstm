%% Add gaussian white noise to data

load('../data/screwdriving_class_type.mat');


%% Configuration
snr=20; % in db


%% Generate data points so all classes have the same number of observations
counts=histcounts(Y);
classes=categorical(unique(Y));
maxCount=max(counts);

XNew={};
YNew={};
for i=1:numel(classes)
    class=classes(i);
    count=counts(i);
    diff=maxCount-count;
    if diff == 0
        continue
    end
    
    n=round(maxCount/count);
    x=X(Y == class);
    y=Y(Y == class);
    for j=1:numel(x)
        if n <= 1
            continue
        end
        xfull=repelem(x{j}, n-1, 1);
        yfull=repelem(y(j), n-1, 1);
        xfull=awgn(xfull,snr);
        c=mat2cell(xfull, repmat(size(x{j},1),n-1,1));
        c=[x{j}; c];
        yfull=[y(j); yfull];
        assert(size(c,1) == size(yfull,1));
        
        XNew=[XNew;c];
        YNew=[YNew;yfull];
    end
end

X=[X;XNew];
Y=[Y;YNew];

%% Save extended X and Y
save('../data/screwdriving_class_type_wgn.mat', 'X','Y','-v7.3');