# Screwdriving models

Classification models trained with LSTM.


## Files

* ```success_fail_classifier``` classifies a sequenece of meatures to 1(success), or 0(failure)
    * ```mk_data.m``` reads all CSV files in a given folder and produces X (features) and Y (labels)
    * ```inspect_data.m``` reads X and Y, and produces a sequences length distribution plot
    * ```slove.m``` splits X and Y to training, validation and test sets, and trains an LSTM model, 
    and reports accuracy on the test data.
    * ```screwdriving_lstm_model.mat``` is the trained model
    * ```predict.m``` predicts the class on the test data using the LSTM model
* ```type_classifier``` calssifies a sequence of meatures to 7 casses: 1(success), or 2-7(failure)
    * ```mk_data.m``` reads all CSV files in a given folder and produces X (features) and Y (labels)
    * ```slove.m``` splits X and Y to training, validation and test sets, and trains an LSTM model, 
    and reports accuracy on the test data.
    * ```screwdriving_type_lstm_model.mat``` is the trained model
* ```results``` figures and results
