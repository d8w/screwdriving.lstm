function acc = accuracy(XTestSub, YTest, net, miniBatchSize)
%accuracy Calucate the classification accuracy of the predictions
%   Detailed explanation goes here

%% Test LSTM Network
% Sort Data by Sequence Length
% During training, the software splits the training data into mini-batches 
% and pads or truncates the sequences so that they have the same length. 
% Too much padding or discarding of data can have a negative impact on 
% the network performance.

numObservationsTest = numel(XTestSub);
sequenceLengthsTest = zeros(numObservationsTest, 1);
for i=1:numObservationsTest
    sequence = XTestSub{i};
    sequenceLengthsTest(i) = size(sequence,2);
end
[sequenceLengthsTest,idx] = sort(sequenceLengthsTest);
XTestSub = XTestSub(idx);
YTest = YTest(idx);


%% Classify the test data.

YPred = classify(net,XTestSub, ...
    'MiniBatchSize',miniBatchSize);


%% Calculate the classification accuracy of the predictions.

acc = sum(YPred == YTest)./numel(YTest);
end

