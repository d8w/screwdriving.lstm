function [P] = subset_fixed(X, len)
%SUBSET_FIXED subset fixed length of X 
%   

P = cell(length(X), 1);
for k=1:length(X)
    x=X{k};
    
    if len > size(x, 2)
        P{k} = x;
    else
        P{k} = x(:, 1:len);
    end
end

end