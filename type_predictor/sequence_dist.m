function [] = sequence_dist(X)
%SEQUENCE_DIST Get the sequence lengths for each observation
%   Detailed explanation goes here
numObservations = numel(X);
sequenceLengths = zeros(numObservations, 1);
for i=1:numObservations
    sequence = X{i};
    sequenceLengths(i) = size(sequence,2);
end

% Sort the data by sequence length.

[sequenceLengths,idx] = sort(sequenceLengths);
X = X(idx);

% View the sorted sequence lengths in a bar chart.

longestObservation = max(cellfun('length', X));

figure
bar(sequenceLengths)
ylim([0 longestObservation])
xlabel("Sequence")
ylabel("Length")
title("Sequence Lengths")
end

