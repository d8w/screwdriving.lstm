function P = subset_pct(X, proportion)
%SUBSET_PCT subset proportion % of X 
%   

P = cell(length(X), 1);
for k=1:length(X)
    x=X{k};
    
    if proportion < 0 || proportion > 1
        P{k} = x;
    else
        len = round(size(x,2) * proportion);
        P{k} = x(:, 1:len);
    end
end

end

