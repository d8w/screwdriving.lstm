%% Load data

load('screwdriving_class_type.mat')


%% Load the trained model

load('screwdriving_type_lstm_model.mat')


%% Divide the data to three sets: train, validation, test

rng(20171129) % for reproducibility
[trainInd, valInd, testInd] = dividerand(size(X,1) , 0.6, 0.2, 0.2);
XTrain = X(trainInd);
YTrain = Y(trainInd);
XVal = X(valInd);
YVal = Y(valInd);
XTest = X(testInd);
YTest = Y(testInd);

miniBatchSize = 25;


%% Subset XTest 1
% Method 1: subset the sequence by percentage

proportions=[.1 .2 .3 .4 .5 .6 .7 .8 .9 .91 .92 .93 .94 .95 .96 .97 .98 .99 1];
acc = zeros(size(proportions));
for k = 1:numel(proportions)
    p = proportions(k);
    XTestSub = subset_pct(XTest, p);
    acc(k) = accuracy(XTestSub, YTest, net, miniBatchSize);
end


%% View the classification accuracy on the subsets of the data

figure
plot(proportions, acc, '-o')
xlabel("Subset pct")
ylabel("Accuracy")


%% Subset XTest 2
% Method 2: subset the sequence by a fixed length

%sequence_dist(XTest)
fixedLen = 1000:500:4000;
acc = zeros(size(fixedLen, 2), 1);
for k = 1:numel(fixedLen)
    len = fixedLen(k);
    XTestSub = subset_fixed(XTest, len);
    acc(k) = accuracy(XTestSub, YTest, net, miniBatchSize);
end


%% View the classification accuracy on the subsets of the data

figure
plot(fixedLen, acc, '-o')
xlabel("Subset length")
ylabel("Accuracy")

