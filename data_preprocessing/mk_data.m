%% Load data from csv and produce X and Y of training and validation.

root='/data/data/foxconn/imputed_with_stages/';
if exist(root, 'dir') ~= 7
    disp(['No such directory: ', root])
    return
end

stageField={'class_stage'};
typeField={'class_type'};
originalFeatureFields={'scalar_fx';'scalar_fy';'scalar_fz';'scalar_tx';...
    'scalar_ty';'scalar_tz';'scalar_current';'scalar_ang_vel'};
featureVars={'scalar_current';'scalar_ang_vel';'scalar_fx';'scalar_fy';
    'scalar_fz';'scalar_tx';'scalar_ty';...
    'scalar_tz';'scalar_fx_sma_0_3_';'scalar_fy_sma_0_3_';...
    'scalar_fz_sma_0_3_';'scalar_tx_sma_0_3_';'scalar_ty_sma_0_3_';...
    'scalar_tz_sma_0_3_';'scalar_fx_sd_0_3_';'scalar_fy_sd_0_3_';...
    'scalar_fz_sd_0_3_';'scalar_tx_sd_0_3_';'scalar_ty_sd_0_3_';...
    'scalar_tz_sd_0_3_';'scalar_current_sma_0_3_';'scalar_ang_vel_sma_0_3_';...
    'scalar_current_sd_0_3_';'scalar_ang_vel_sd_0_3_'};

src=strcat(root, '*.csv');
X = cell(1862,1);
Xorig = cell(1862,1);
YStage = zeros(1862,1);
YType = zeros(1862,1);
files = dir(src);
for k = 1:length(files)
    baseFileName = files(k).name;
    fullFileName = fullfile(root, baseFileName);
    M = readtable(fullFileName);
    X{k} = M{:, featureVars}';
    Xorig{k} = M{:, originalFeatureFields}';
    YStage(k) = M{end, stageField}; % use the last stage as it's what to predict
    YType(k) = M{1, typeField};
end

YStage = categorical(YStage);
YType = categorical(YType);

filename='../data/screwdriving_stage_type.mat';
save(filename, 'X', 'YStage', 'YType');
disp(['Saved data to ', filename])

filename='../data/screwdriving_original_stage_type.mat';
X=Xorig;
save(filename, 'X', 'YStage', 'YType');
disp(['Saved data to ', filename])